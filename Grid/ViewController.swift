//
//  ViewController.swift
//  Grid
//
//  Created by Ivan Kh on 19/08/2017.
//  Copyright © 2017 Ivan Kh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var buttonDone: UIBarButtonItem!
    @IBOutlet weak var buttonSubmit: UIBarButtonItem!
    
    var productCostController: ProductCostController? {
        return self.childViewControllers.first as? ProductCostController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: .UIKeyboardWillHide, object: nil)
        
        // Example of changing rows and columns count
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.productCostController?.columnCount = 2
            self.productCostController?.rowCount = 3
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonSubmitAction(_ sender: Any) {
        productCostController?.submit()
    }
    
    @IBAction func buttonDoneAction(_ sender: Any) {
        productCostController?.editingCell?.textField.resignFirstResponder()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        buttonDone.isEnabled = true
    }

    func keyboardWillHide(notification: NSNotification) {
        buttonDone.isEnabled = false
    }

}

