//
//  ProductCostCell.swift
//  Grid
//
//  Created by Ivan Kh on 19/08/2017.
//  Copyright © 2017 Ivan Kh. All rights reserved.
//

import UIKit

let defaultColorComponent: CGFloat = 109.0 / 255.0

class ProductCostHeaderCell : UICollectionViewCell {

    var label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepare()
    }
    
    func prepare() {
        label.frame = self.bounds
        label.textColor = UIColor(red: defaultColorComponent,
                                  green: defaultColorComponent,
                                  blue: defaultColorComponent,
                                  alpha: 1.0)
        addSubview(label)
    }
}

class ProductCostCell : UICollectionViewCell, UITextFieldDelegate {
    var textField = UITextField()
    var borderBottom = UIView()
    var begin: ((ProductCostCell) -> Void)?
    var end: ((ProductCostCell) -> Void)?
    var action: ((ProductCostCell) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepare()
    }
    
    func prepare() {
        textField.text = ProductCostCellDefaultValue
        textField.delegate = self

        borderBottom.backgroundColor = UIColor(red: defaultColorComponent,
                                               green: defaultColorComponent,
                                               blue: defaultColorComponent,
                                               alpha: 1.0)
        
        contentView.addSubview(textField)
        contentView.addSubview(borderBottom)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        textField.frame = self.bounds

        borderBottom.frame = self.bounds
        borderBottom.frame.origin.y = borderBottom.frame.size.height - 1.0
        borderBottom.frame.size.height = 1.0
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text == ProductCostCellDefaultValue {
            DispatchQueue.main.async { textField.selectAll(nil) }
        }
        
        begin?(self)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        end?(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        action?(self)
        return false
    }
}
