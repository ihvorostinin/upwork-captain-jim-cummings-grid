//
//  ProductCostController.swift
//  Grid
//
//  Created by Ivan Kh on 19/08/2017.
//  Copyright © 2017 Ivan Kh. All rights reserved.
//

import UIKit

fileprivate let cellHeaderID = "header"
fileprivate let cellDefaultID = "default"

class ProductCostController : UICollectionViewController {

    var editingCell: ProductCostCell?
    var data = [String]()
    
    var columnCount = ProductCostColumnCount {
        didSet {
            updateColumnsAndRows()
        }
    }
    
    var rowCount = ProductCostRowCount {
        didSet {
            updateColumnsAndRows()
        }
    }
    
    var textField = UITextField()
    
    init() {
        super.init(collectionViewLayout: ProductCostLayout())
    }
 
    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func updateColumnsAndRows() {
        data = [String](repeating: ProductCostCellDefaultValue, count: rowCount * columnCount)

        guard let layout = self.collectionView?.collectionViewLayout as? ProductCostLayout else { return }
        
        layout.rowCount = rowCount
        layout.columnCount = columnCount
        
        collectionView?.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.register(ProductCostHeaderCell.self, forCellWithReuseIdentifier: cellHeaderID)
        collectionView?.register(ProductCostCell.self, forCellWithReuseIdentifier: cellDefaultID)
        collectionView?.collectionViewLayout = ProductCostLayout()
        collectionView?.isScrollEnabled = false
        collectionView?.keyboardDismissMode = .interactive
        
        collectionView?.addSubview(textField)
        
        updateColumnsAndRows()
    }
    
    func submit() {
        guard let collectionView = self.collectionView else { return }
        
        var result = String()
        
        if let editingCell = editingCell {
            let indexPath = collectionView.indexPath(for: editingCell)!
            
            self.data[indexPath.row - columnCount]
                = editingCell.textField.text
                ?? ProductCostCellDefaultValue
        }

        for i in 0 ..< data.count {
            result += data[i]
            
            if i != data.count - 1 {
                result += ProductCostResultSeparator
            }
        }
        
        UIPasteboard.general.string = result
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return (rowCount + 1) * columnCount
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row < columnCount {
            let result
                = collectionView.dequeueReusableCell(withReuseIdentifier: cellHeaderID, for: indexPath)
                    as! ProductCostHeaderCell
            
            result.label.text = ProductCostHeaders[indexPath.row]
            
            return result
        }
        else {
            let result
                = collectionView.dequeueReusableCell(withReuseIdentifier: cellDefaultID, for: indexPath)
                    as! ProductCostCell
            
            result.textField.text = data[indexPath.row - columnCount]
            
            result.begin = { cell in
                self.editingCell = cell
            }
            
            result.end = { cell in
                self.data[indexPath.row - self.columnCount] = cell.textField.text ?? ProductCostCellDefaultValue
                
                if self.editingCell == cell {
                    self.editingCell = nil
                }
            }
            
            result.action = { cell in
                
                if indexPath.row + 1 < collectionView.numberOfItems(inSection: 0) {
                    let nextCell
                        = collectionView.cellForItem(at: IndexPath(row: indexPath.row + 1, section: 0))
                            as! ProductCostCell
                    
                    nextCell.textField.becomeFirstResponder()
                }
                else if self.rowCount + 1 < ProductCostRowMaxCount {
                    var indexPaths = [IndexPath]()
                    
                    for i in 1 ..< self.columnCount + 1 {
                        indexPaths.append(IndexPath(row: indexPath.row + i, section: 0))
                    }
                    
                    self.rowCount += 1
                    collectionView.insertItems(at: indexPaths)
                    
                    let nextCell = collectionView.cellForItem(at: indexPaths.first!) as! ProductCostCell
                    nextCell.textField.becomeFirstResponder()
                }
                else {
                    cell.textField.resignFirstResponder()
                }
            }
            
            return result
        }
    }    
}
