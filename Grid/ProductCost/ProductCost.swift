//
//  ProductCost.swift
//  Grid
//
//  Created by Ivan Kh on 19/08/2017.
//  Copyright © 2017 Ivan Kh. All rights reserved.
//

import UIKit

let ProductCostColumnCount = 3
let ProductCostHeaders = ["Fixed Cost", "Variable Cost", "Unit Price"]
let ProductCostRowCount = 10
let ProductCostRowMaxCount = 10
let ProductCostCellRowHeight: CGFloat = 25.0
let ProductCostCellSpaceX: CGFloat = 10.0
let ProductCostCellSpaceY: CGFloat = 10.0
let ProductCostCellDefaultValue = "0"
let ProductCostResultSeparator = ","
