//
//  ProductCostView.swift
//  Grid
//
//  Created by Ivan Kh on 19/08/2017.
//  Copyright © 2017 Ivan Kh. All rights reserved.
//

import UIKit

class ProductCostLayout : UICollectionViewFlowLayout {
        
    var columnCount: Int = 0
    var rowCount: Int = 0

    override var collectionViewContentSize: CGSize {
        guard let collectionView = self.collectionView else { return CGSize.zero }
        
        var result = collectionView.frame.size
        result.height
            = CGFloat(collectionView.numberOfItems(inSection: 0) / columnCount)
            * (ProductCostCellRowHeight + ProductCostCellSpaceY) - ProductCostCellSpaceY
        return result
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard
            let collectionView = self.collectionView
            else { return nil }

        let maxCount = rowCount * columnCount + columnCount
        var startIndex = Int(rect.origin.y / (ProductCostCellRowHeight + ProductCostCellSpaceY))
        var endIndex = Int((rect.origin.y + rect.size.height) / (ProductCostCellRowHeight + ProductCostCellSpaceY))
        
        startIndex *= columnCount
        endIndex *= columnCount
        
        startIndex = max(startIndex/* - columnCount*/, 0)
        startIndex = min(startIndex, maxCount)
        endIndex = min(endIndex /*+ columnCount*/, maxCount)
        endIndex = max(endIndex, 0)
        
        
        let width
            = (collectionView.frame.size.width - ProductCostCellSpaceX * CGFloat(columnCount - 1))
            / CGFloat(columnCount)
        let height = CGFloat(ProductCostCellRowHeight)
        var result = [UICollectionViewLayoutAttributes]()
        
        for i in startIndex ..< endIndex {
            let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(row: i, section: 0))
            let indexInRow = attributes.indexPath.row % columnCount
            let row = (attributes.indexPath.row - indexInRow) / columnCount
            
            attributes.size.width = width
            attributes.size.height = height
            attributes.frame.origin.x = width * CGFloat(indexInRow) + (CGFloat(ProductCostCellSpaceX) * CGFloat(indexInRow))
            attributes.frame.origin.y = CGFloat(row) * height + (CGFloat(ProductCostCellSpaceY) * CGFloat(row))
            
            result.append(attributes)
        }
        
        return result
    }
}
